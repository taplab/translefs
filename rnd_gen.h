#ifndef RND_GEN_H
#define RND_GEN_H

#include <random>
#include "time.h"

static std::mt19937 rnd_gen(time(0));
static std::uniform_real_distribution<double> udist01(0,1);

/*setup seed generator (mt19937 gives numbers > max langevin seed (< INT_MAX), hence we need to 
generate seeds with seed_dist*/
static int max_seed = 100000000;   //randomly choosen
static std::uniform_int_distribution<int> seed_dist(1, max_seed);

#endif
