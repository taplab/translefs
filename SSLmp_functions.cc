#include "Slip_Spring.h"
#include "SSLmp_functions.h"
#include "rnd_gen.h"

#include <vector>
#include <random>
#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

int abs(int x) {
	if(x >= 0) return x;
	else return (-x);
}


void move_slip_springs(int Nb, LAMMPS_NS::LAMMPS *lmp, vector<Slip_Spring*> &SSprings, double speed, double loading_rate, double unloading_rate, int time, ofstream& ofile) {

	double *data = new double[3*Nb];
	int *ids = new int[Nb];
	int *types = new int[Nb];
	int** moved_SS = new int*[2];
	for(int i = 0; i < 2; i++)  moved_SS[i] = new int[SSprings.size()];
	for(int i = 0; i < 2; i++) for(int j = 0; j < SSprings.size(); j++) moved_SS[i][j] = 0;

	double x1[3];
	double x2[3];
	double x[3];

	vector<int> jump_ids;
	vector<int> v;
	
	double p1u,p1l,p1r,dd;
	int pos,b1,b2,b1old,b2old,a,h,aold,hold;
	int overlp=0;

	double pr,dir_pr;
	char dir;

	stringstream line;

	lammps_gather_atoms(lmp, "x", 1, 3, data);

	lammps_gather_atoms(lmp, "type", 0, 1, types);

	lammps_gather_atoms(lmp, "id", 0, 1, ids);

	//shuffle(vector<Slip_Spring*> &SSprings); //Randomly sort SSprings


	for(int i = 0; i < SSprings.size(); i++) {

		// IF SS IS LOADED

		if(SSprings[i]->is_active()) {

			aold=SSprings[i]->get_anchor();
			hold=SSprings[i]->get_hook();

			//////NEW Resetting types
			types[aold-1] = 1; types[hold-1] = 1;
			types[aold] = types[aold-2] = types[hold] = types[hold-2] = 1;	//NEW type 3
			types[aold+1] = types[aold-3] = types[hold+1] = types[hold-3] = 1;  //NEW type 3
			//////////////

			/*//PBC
			if(b1==0)b1=Nbeads;
			if(b2==Nbeads+1)b2=1; */

			// UNLOAD

			p1u=udist01(rnd_gen);
			//cout << "random seed " << p1u << " vs " << unloading_rate <<endl;
			if(p1u<unloading_rate) {

				line << "group temp id " << aold << " " << hold;
				lmp->input->one(line.str().c_str());
				
				line.str("");
				line.clear();

				lmp->input->one("delete_bonds temp multi remove");	//remove bond
				lmp->input->one("group temp delete");

				SSprings[i]->unload();
				continue;
			}

			// OR MOVE

			p1r=udist01(rnd_gen);
	
			if(p1r<speed) {

				bool inter_strand = false;

				a=aold;
				h=hold;

				x1[0] = data[3*(a-1)]; x1[1] = data[3*(a-1)+1]; x1[2] = data[3*(a-1)+2];
				x2[0] = data[3*(h-1)]; x2[1] = data[3*(h-1)+1]; x2[2] = data[3*(h-1)+2];

				for(int j = 0; j < Nb; j++) {
			
					x[0] = data[3*j]; x[1] = data[3*j+1]; x[2] = data[3*j+2];
					dd = dist(x, x1);
					if(dd < 3.4) {
						if(abs(j-h)<5) {	//same strand
							if(SSprings[i]->get_dir()=='d') {if(j < h-1) jump_ids.push_back(j+1);}
							else if(SSprings[i]->get_dir()=='u') {if(j > h-1) jump_ids.push_back(j+1);}
						}
						else if(abs(j-a)>5 && udist01(rnd_gen)<0.005) jump_ids.push_back(j+1);	//intra strand
					}
				}

				if(jump_ids.empty()) continue;
				h = jump_ids[(int) (udist01(rnd_gen)*jump_ids.size())]; 
				jump_ids.clear();
				if(abs(h-hold)>=10) inter_strand = true;


				moved_SS[0][i]=1;	//move attempted

				//overlaps
				if(a==h || a==h+1 || a==h-1) continue;
				if(a==aold && h==hold) 	continue;

				overlp=overlap(a,h,Nb,SSprings,i);

				if(overlp!=0) continue;
			
				//move SS
				SSprings[i]->move(h);

				//randomly change direction if inter strand event occurred
				if(inter_strand == true) {
					if(udist01(rnd_gen)<0.5) SSprings[i]->change_dir('u');
					else SSprings[i]->change_dir('d');
				}

				moved_SS[1][i]=abs(h-hold);	//jumped distance

				cout << "enlarging bond " << i << " to " << a << " -- " << h << endl;// cin.get();
			
				//***CHANGING BONDs in lammps***

				if(h>a) {
					if(abs(a-h)>6) line << "create_bonds single/bond 2 " << a << " " << h;   //add new bond of type 2
					else if(abs(a-h)<=6) line << "create_bonds single/bond 3 " << a << " " << h;   //add new bond of type 3
				}
				else {
					if(abs(a-h)>6) line << "create_bonds single/bond 2 " << h << " " << a;   //add new bond of type 2
					else if(abs(a-h)<=6) line << "create_bonds single/bond 3 " << h << " " << a;   //add new bond of type 3
				}
				lmp->input->one(line.str().c_str());


				line.str("");
				line.clear();

				line << "group temp id " << aold << " " << hold;
				lmp->input->one(line.str().c_str());
				
				line.str("");
				line.clear();

				lmp->input->one("delete_bonds temp multi remove");	//delete old bond
				lmp->input->one("group temp delete");

				cout << "SS " << i <<" moved" << endl;
				
			}
		}

		//IF IT IS ULOADED

		else{

			p1l=udist01(rnd_gen);

			if(p1l<loading_rate) {

				int id, pos2;

				//int cnt = 0;
				while(1==1) {
					
					pos = (int) (5+udist01(rnd_gen)*(Nb-10));	//Condensin 1 is loaded between 5 and Nb-5

					x1[0] = data[3*(pos-1)]; x1[1] = data[3*(pos-1)+1]; x1[2] = data[3*(pos-1)+2];
					for(int j = 0; j < Nb; j++) {
						x2[0] = data[3*j]; x2[1] = data[3*j+1]; x2[2] = data[3*j+2];					
						dd=dist(x1,x2);
						if(dd < 3.4) {
							if(abs(pos-j-1)<=3) v.push_back(j+1);
							else if(udist01(rnd_gen)<0.005) v.push_back(j+1);
						}
					}

					if(v.empty()) continue;

					id = (int) (-0.5+udist01(rnd_gen)*v.size());
					pos2 = v[id];
					
					v.clear();

					if(pos<=pos2) {b1=pos;b2=pos2;}
					else {b1=pos2;b2=pos;}
					if(abs(b2-b1)<2) continue;
					
					if((overlap(b1,b2,Nb,SSprings,i)==0)) break; //check overlaps with other SSs or DNA ends

				}					


				dir_pr = udist01(rnd_gen);

				if(dir_pr < 0.5) dir = 'u';
				else dir = 'd';


				if(dir == 'd') {
					h = b1; a = b2;
				}
				else {
					h = b2; a = b1;
				}				

				cout << "loading " << i << ": a = " << a << " " << " (" << data[3*(a-1)] << " " << data[3*(a-1)+1] << " " << data[3*(a-1)+2] 
						<< ") h = " << h << " (" << data[3*(h-1)] << " " << data[3*(h-1)+1] << " " << data[3*(h-1)+2] << ")" << endl;


		

				//cout << b1 << " " << b2 << endl;		

				SSprings[i]->load(a,h,dir);

				cout << "SS " << i <<" loaded; dir = " << dir << endl;

				line << "create_bonds single/bond 3 " << b1 << " " << b2;   //add new bond of type 3 (b1<b2)
				lmp->input->one(line.str().c_str());

				line.str("");
				line.clear();

			}
			else continue;
		}
	}

///////////////////NEW	changing types of SS-ed beads
	for(int i = 0; i < SSprings.size(); i++) {
		if(SSprings[i]->is_active()) {

			a = SSprings[i]->get_anchor();
			h = SSprings[i]->get_hook();

			types[a]=types[a-2]=types[h]=types[h-2]=3;
			types[h-1] = 2;
			types[a-1] = 2;

			lammps_scatter_atoms(lmp, "type", 0, 1, types);
		}
	}


///////////////
	
	bool attempted = false;
	for(int i = 0; i < SSprings.size(); i++) if( moved_SS[0][i]==1 ) attempted = true;
	if(attempted) {
		ofile << time;
		for(int i = 0; i < SSprings.size(); i++) {
			if(moved_SS[0][i]==0) ofile << " n"; 
			else ofile << " " << moved_SS[1][i];
		}
		ofile << endl;
	}

	delete[] data;
	delete[] ids;
	delete[] types;
	for(int i = 0; i < 2; i++) delete[] moved_SS[i];
	delete[] moved_SS;
}

//overlaps checks if there is any overlap between a given ring of the vector rings
//(the one at the position r_id of the vector) and everything else: 
//the polymer, the cohesins and the other rings.

int overlap(int b1, int b2, int Nb, vector<Slip_Spring*> &SSprings, int sn) {

	bool ol1 = false;
	bool ol2 = false;

	for(int i = 0; i < SSprings.size(); i++) {
		if(i==sn) {
			if((b1<3)||(b1>Nb-3)) ol1 = true;
			if((b2<3)||(b2>Nb-3)) ol2 = true;
		}
		else {
			if((abs(SSprings[i]->get_anchor()-b1)<2)||(abs(SSprings[i]->get_hook()-b1)<2)) ol1=true;
			if((abs(SSprings[i]->get_anchor()-b2)<2)||(abs(SSprings[i]->get_hook()-b2)<2)) ol2=true;
		}
	}

	if(ol1 && ol2) return 3;
	else if (ol1) return 1;
	else if (ol2) return 2;
	else return 0;
}


double dist(double *u, double *v) {

	double d = sqrt( (u[0]-v[0])*(u[0]-v[0])+(u[1]-v[1])*(u[1]-v[1])+(u[2]-v[2])*(u[2]-v[2]) );
	return( d );

}


